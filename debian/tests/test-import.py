#!/usr/bin/env python2

import requests
from colorama import Fore
from colorama import Back
from colorama import Style

import imaplib
import sys
import smtplib
import urllib2
import re
import bs4
import hashlib
import zipfile
import cStringIO
import itertools
import string
import urllib3
import urllib
import random
import time
import argparse
import sqlite3

import soupsieve

response = requests.get('https://www.kali.org')
print("Status is: %s" % response.status_code)
